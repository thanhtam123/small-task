## Description

A simple task management api using nodejs, express, typescript and mongodb.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev

# production
$ npm start
```

## Note

Before using the api, you need to register (using _auth/register_ route) and login (using _auth/login_ route) to get **x-access-token**.

To use all board, list and task route, you must have **x-access-token** in request header, as well as boardId in request body except _board/:id_ route.
