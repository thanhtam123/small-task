import * as mongoose from 'mongoose';
import UserInterface from './user.interface';
import * as bcrypt from 'bcrypt';

const userSchema = new mongoose.Schema(
	{
		username: {
			type: String,
			unique: true,
			trim: true,
			required: true,
		},
		password: {
			type: String,
			minlength: 8,
			trim: true,
			required: true,
		},
	},
	{
		timestamps: true,
	},
);

/**
 * pre save hook
 */
userSchema.pre('save', async function <UserInterface>() {
	const admin = this;

	//hash password
	if (admin.isModified('password')) {
		this.password = await bcrypt.hash(admin.password, 8);
	}
});

/**
 * check if input password is match with admin's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
userSchema.methods.isPasswordMatch = async function <UserInterface>(
	password: string,
): Promise<boolean> {
	const admin = this;
	return await bcrypt.compare(password, admin.password);
	// return password == admin.password;
};

const UserModel = mongoose.model<UserInterface & mongoose.Document>(
	'User',
	userSchema,
);
export default UserModel;
