import { IsNotEmpty, IsOptional, IsString, MinLength } from 'class-validator';

export default class CreateUserDto {
	@IsNotEmpty()
	@IsString()
	username: string;

	@IsNotEmpty()
	@IsString()
	@MinLength(8)
	password: string;
}
