export default interface UserInterface {
	_id?: string;
	username: string;
	password: string;
	createdAt: Date;
	updatedAt: String;

	isPasswordMatch(password: string): Promise<boolean>;
}
