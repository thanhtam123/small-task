import { Router, NextFunction, Request, Response } from 'express';
import catchAsync from '../utils/catchAsync';
import UserService from './user.service';
import StatusCodes from '../utils/httpStatusCode';
import ControllerInterface from 'interfaces/controller.interface';
import validationMiddleware from '../middlewares/validation';
import UpdateUserDto from './dto/user-update.dto';
import { verifyToken } from '../middlewares/auth.middleware';
import RequestWithUser from '../interfaces/requestWithUser.interface';
import ApiError from '../utils/ApiError';

export default class UserController implements ControllerInterface {
	public path = '/user';
	public router = Router();
	private userService: UserService;

	constructor(userService: UserService) {
		this.userService = userService;
		this.intializeRoutes();
	}

	public intializeRoutes() {
		// this.router.post(
		// 	'/',
		// 	validationMiddleware(CreateUserDto),
		// 	this.createUser,
		// );
		this.router.get('/', this.findAll);
		this.router.delete('/:id', this.deleteById);

		this.router.get('/:id', verifyToken, this.findById);
		this.router.put(
			'/:id',
			verifyToken,
			validationMiddleware(UpdateUserDto, true),
			this.updateById,
		);
	}

	private createUser = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const user = await this.userService.create(req.body);
			delete user.password;
			res.status(StatusCodes.CREATED).send(user);
		},
	);

	private findById = catchAsync(
		async (req: RequestWithUser, res: Response, next: NextFunction) => {
			if (req.user._id != req.params.id) {
				throw new ApiError(StatusCodes.UNAUTHORIZED, 'Unauthorized.');
			}
			const user = await this.userService.findById(req.params.id);
			res.status(StatusCodes.OK).send(user);
		},
	);

	private findAll = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const users = await this.userService.findAll();
			res.status(StatusCodes.OK).send(users);
		},
	);

	private updateById = catchAsync(
		async (req: RequestWithUser, res: Response, next: NextFunction) => {
			if (req.user._id != req.params.id) {
				throw new ApiError(StatusCodes.UNAUTHORIZED, 'Unauthorized.');
			}
			const users = await this.userService.updateById(
				req.params.id,
				req.body,
			);
			res.status(StatusCodes.OK).send(users);
		},
	);

	private deleteById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			await this.userService.deleteById(req.params.id);
			res.status(StatusCodes.OK).send({
				message: 'User was deleted successfully.',
			});
		},
	);
}
