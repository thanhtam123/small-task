import UserModel from './user.model';
import ApiError from '../utils/ApiError';
import StatusCodes from '../utils/httpStatusCode';
import CreateUserDto from './dto/user-create.dto';

export default class UserService {
	private User = UserModel;

	async create(userBody: CreateUserDto) {
		const { username } = userBody;
		const user = await this.User.findOne({ username });
		if (user) {
			throw new ApiError(
				StatusCodes.BAD_REQUEST,
				`Username ${username} is already exist.`,
			);
		}

		const newUser = await this.User.create(userBody);
		return newUser;
	}

	async loginWithUsernameAndEmail(username: string, password: string) {
		const user = await this.User.findOne({ username });
		if (!user || !user.isPasswordMatch(password)) {
			throw new ApiError(
				StatusCodes.BAD_REQUEST,
				'Incorrect username or password.',
			);
		}
		return user;
	}

	async findAll() {
		const users = await this.User.find({});
		return users;
	}

	async findById(id: string) {
		const user = await this.User.findById(id);
		if (!user) {
			throw new ApiError(
				StatusCodes.BAD_REQUEST,
				`Not found user with id ${id}`,
			);
		}
		return user;
	}

	async updateById(id: string, updateBody: any) {
		const user = await this.User.findById(id);
		if (!user) {
			throw new ApiError(
				StatusCodes.BAD_REQUEST,
				`Not found user with id ${id}`,
			);
		}

		Object.assign(user, updateBody);
		await user.save();
		return user;
	}

	async deleteById(id: string) {
		return this.User.findByIdAndRemove(id)
			.then((user: any) => {
				if (!user) {
					throw new ApiError(
						StatusCodes.NOT_FOUND,
						`Cannot delete user with id=${id}. Maybe Tutorial was not found!`,
					);
				} else {
					return true;
				}
			})
			.catch((err: Error) => {
				throw new ApiError(
					StatusCodes.INTERNAL_SERVER_ERROR,
					`Error when deleting user with id ${id}`,
				);
			});
	}
}
