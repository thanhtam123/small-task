import { Request } from 'express';
import UserInterface from '../users/user.interface';

export default interface RequestWithAny extends Request {
	user?: UserInterface;
	isLogin?: boolean;
}
