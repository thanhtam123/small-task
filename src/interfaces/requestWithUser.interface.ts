import { Request } from 'express';
import UserInterface from '../users/user.interface';

export default interface RequestWithUser extends Request {
	user: UserInterface;
}
