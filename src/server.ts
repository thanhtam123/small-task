import App from './app';
import 'dotenv/config';
import envValidate from './utils/validateEnv';

import {
	boardController,
	listController,
	taskController,
} from './boards/board.module';
import { userController } from './users/user.module';
import { authenticationController } from './authentication/authentication.module';

envValidate();

const controllers = [
	authenticationController,
	userController,
	boardController,
	listController,
	taskController,
];

const app = new App(controllers);
app.listen();
