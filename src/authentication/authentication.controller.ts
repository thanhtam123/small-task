import { Router, Request, Response, NextFunction } from 'express';
import catchAsync from '../utils/catchAsync';
import ControllerInterface from '../interfaces/controller.interface';
import RegisterDto from './dto/register.dto';
import LoginDto from './dto/login.dto';
import validationMiddleware from '../middlewares/validation';
import StatusCodes from '../utils/httpStatusCode';
import AuthenticationService from './authentication.service';

export default class AuthenticationController implements ControllerInterface {
	public path = '/auth';
	public router = Router();
	private authenticationService: AuthenticationService;

	constructor(authenticationService: AuthenticationService) {
		this.authenticationService = authenticationService;

		this.initializeRoutes();
	}

	private initializeRoutes() {
		this.router.post(
			'/register',
			validationMiddleware(RegisterDto),
			this.register,
		);
		this.router.post('/login', validationMiddleware(LoginDto), this.login);
	}

	private register = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const user = await this.authenticationService.register(req.body);
			res.status(StatusCodes.CREATED).send(user);
		},
	);

	private login = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { username, password } = req.body;
			const user = await this.authenticationService.login(
				username,
				password,
			);

			const tokenData = this.authenticationService.createToken({
				_id: user._id,
			});
			res.status(StatusCodes.OK).send({ user, tokenData });
		},
	);
}
