import { userService } from '../users/user.module';
import AuthenticationController from './authentication.controller';
import AuthenticationService from './authentication.service';

export const authenticationService = new AuthenticationService(userService);
export const authenticationController = new AuthenticationController(
	authenticationService,
);
