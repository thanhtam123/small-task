import UserService from 'users/user.service';
import RegisterDto from './dto/register.dto';
import DataStoredInToken from './interfaces/dataStoredInToken';
import TokenData from './interfaces/tokenData.interface';
import * as jwt from 'jsonwebtoken';

export default class AuthenticationService {
	private userService: UserService;

	constructor(userService: UserService) {
		this.userService = userService;
	}

	async register(userBody: RegisterDto) {
		const user = await this.userService.create(userBody);
		user.password = undefined;
		return user;
	}

	async login(username: string, password: string) {
		const user = await this.userService.loginWithUsernameAndEmail(
			username,
			password,
		);
		user.password = undefined;
		return user;
	}

	createCookie(tokenData: TokenData) {
		return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn}`;
	}

	createToken(payload: DataStoredInToken): TokenData {
		const secret = process.env.JWT_SECRET;
		const expiresIn = Number(process.env.JWT_EXPIRATION_TIME);
		return {
			expiresIn,
			token: jwt.sign(payload, secret, { expiresIn }),
		};
	}
}
