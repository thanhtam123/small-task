import BoardService from '../services/board.service';
import ControllerInterface from '../../interfaces/controller.interface';
import { Router, NextFunction, Request, Response } from 'express';
import catchAsync from '../../utils/catchAsync';
import StatusCodes from '../../utils/httpStatusCode';
import CreateBoardDto from '../dto/board-create.dto';
import validationMiddleware from '../../middlewares/validation';
import UpdateBoardDto from '../dto/board-update.dto';
import { verifyTokenV2, verifyAuth } from '../../middlewares/auth.middleware';
import RequestWithAny from '../../interfaces/requestWithAny.interface';
import ApiError from '../../utils/ApiError';

export default class BoardController implements ControllerInterface {
	public path = '/board';
	public router = Router();

	private boardService: BoardService;

	constructor(boardService: BoardService) {
		this.boardService = boardService;

		this.intializeRoutes();
	}

	public intializeRoutes() {
		this.router
			.route('/')
			.post(
				verifyTokenV2(true),
				validationMiddleware(CreateBoardDto),
				this.createBoard,
			)
			.get(this.findAll);

		this.router
			.route('/:id')
			.get(verifyTokenV2(), this.findById)
			.put(
				verifyTokenV2(true),
				verifyAuth,
				validationMiddleware(UpdateBoardDto, true),
				this.updateById,
			)
			.delete(verifyTokenV2(true), verifyAuth, this.deleteById);
	}

	private createBoard = catchAsync(
		async (req: RequestWithAny, res: Response, next: NextFunction) => {
			const board = await this.boardService.create(
				req.body,
				req.user._id,
			);
			res.status(StatusCodes.CREATED).send({
				board,
			});
		},
	);

	private findAll = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const boards = await this.boardService.findAll();
			res.status(StatusCodes.OK).send({ boards });
		},
	);

	private findById = catchAsync(
		async (req: RequestWithAny, res: Response, next: NextFunction) => {
			const { id } = req.params;
			const { board, owner } = await this.boardService.findById(id);

			if (board.published === false) {
				if (req.isLogin === false) {
					throw new ApiError(
						StatusCodes.UNAUTHORIZED,
						`You are not authorized to see this board.`,
					);
				} else {
					const userId = req.user._id;
					if (owner.toString() !== userId.toString()) {
						throw new ApiError(
							StatusCodes.UNAUTHORIZED,
							`You are not authorized to see this board.`,
						);
					}
				}
			}

			res.status(StatusCodes.OK).send({ board });
		},
	);

	private updateById = catchAsync(
		async (req: RequestWithAny, res: Response, next: NextFunction) => {
			const { id } = req.params;
			const board = await this.boardService.updateById(id, req.body);
			res.status(StatusCodes.OK).send({ board });
		},
	);

	private deleteById = catchAsync(
		async (req: RequestWithAny, res: Response, next: NextFunction) => {
			const { id } = req.params;
			await this.boardService.deleteBoard(id);
			res.status(StatusCodes.OK).send({ message: 'Board deleted OK.' });
		},
	);
}
