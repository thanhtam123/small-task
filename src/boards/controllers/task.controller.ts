import { NextFunction, Response, Router, Request } from 'express';
import catchAsync from '../../utils/catchAsync';
import StatusCodes from '../../utils/httpStatusCode';
import ControllerInterface from '../../interfaces/controller.interface';
import validationMiddleware from '../../middlewares/validation';
import TaskService from '../services/task.service';
import CreateTaskDto from '../dto/task-create.dto';
import UpdateTaskDto from '../dto/task-update.dto';
import ChangeListTaskDto from '../dto/task-changelist.dto';
import { verifyTokenV2, verifyAuth } from '../../middlewares/auth.middleware';

export default class TaskController implements ControllerInterface {
	public path = '/task';
	public router = Router();

	private taskService: TaskService;

	constructor(taskService: TaskService) {
		this.taskService = taskService;

		this.intializeRoutes();
	}

	public intializeRoutes() {
		this.router.use(verifyTokenV2(true));
		this.router.use(verifyAuth);
		this.router
			.route('/list/:id')
			.post(validationMiddleware(CreateTaskDto), this.createTask)
			.get(this.findTasksByListId);

		this.router
			.route('/:id')
			.get(this.findById)
			.put(validationMiddleware(UpdateTaskDto), this.updateById)
			.delete(this.deleteById);
		this.router.put(
			'/:id/change-list',
			validationMiddleware(ChangeListTaskDto),
			this.changList,
		);
	}

	private createTask = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const listId = req.params.id;
			const task = await this.taskService.create(req.body, listId);
			res.status(StatusCodes.CREATED).send({ task });
		},
	);

	private findById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { id } = req.params;
			const task = await this.taskService.findById(id);
			res.status(StatusCodes.OK).send({ task });
		},
	);

	private findTasksByListId = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const listId = req.params.id;
			const tasks = await this.taskService.findTasksByListId(listId);
			res.status(StatusCodes.OK).send({ tasks });
		},
	);

	private updateById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { id } = req.params;
			const task = await this.taskService.updateById(id, req.body);
			res.status(StatusCodes.OK).send({ task });
		},
	);

	private deleteById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { id } = req.params;
			await this.taskService.deleteTask(id);
			res.status(StatusCodes.OK).send({ message: 'Task deleted OK.' });
		},
	);

	private changList = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { id } = req.params;
			const { listId, boardId } = req.body;
			const task = await this.taskService.changeList(id, listId, boardId);
			res.status(StatusCodes.OK).send({ task });
		},
	);
}
