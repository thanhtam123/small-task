import { NextFunction, Response, Router, Request } from 'express';
import catchAsync from '../../utils/catchAsync';
import StatusCodes from '../../utils/httpStatusCode';
import ControllerInterface from '../../interfaces/controller.interface';
import ListService from '../services/list.service';
import validationMiddleware from '../../middlewares/validation';
import CreateListDto from '../dto/list-create.dto';
import UpdateBoardDto from '../dto/board-update.dto';
import { verifyTokenV2, verifyAuth } from '../../middlewares/auth.middleware';

export default class ListController implements ControllerInterface {
	public path = '/list';
	public router = Router();

	private listService: ListService;

	constructor(listService: ListService) {
		this.listService = listService;

		this.intializeRoutes();
	}

	public intializeRoutes() {
		this.router.use(verifyTokenV2(true));
		this.router.use(verifyAuth);
		this.router
			.route('/board/:id')
			.post(validationMiddleware(CreateListDto), this.createList)
			.get(this.findListsByBoardId);

		this.router
			.route('/:id')
			.get(this.findById)
			.put(validationMiddleware(UpdateBoardDto), this.updateById)
			.delete(this.deleteById);
	}

	private createList = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const boardId = req.params.id;
			const list = await this.listService.create(req.body, boardId);
			res.status(StatusCodes.CREATED).send({ list });
		},
	);

	private findById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { id } = req.params;
			const list = await this.listService.findById(id);
			res.status(StatusCodes.OK).send({ list });
		},
	);

	private findListsByBoardId = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const boardId = req.params.id;
			const lists = await this.listService.findListsByBoardId(boardId);
			res.status(StatusCodes.OK).send({ lists });
		},
	);

	private updateById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { id } = req.params;
			const lists = await this.listService.updateById(id, req.body);
			res.status(StatusCodes.OK).send({ lists });
		},
	);

	private deleteById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { id } = req.params;
			await this.listService.deleteList(id);
			res.status(StatusCodes.OK).send({ message: 'List deleted OK.' });
		},
	);

	private changList = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { id } = req.params;
			const { listId, boardId } = req.body;
			await this.listService.deleteList(id);
			res.status(StatusCodes.OK).send({ message: 'List deleted OK.' });
		},
	);
}
