import CreateBoardDto from '../dto/board-create.dto';
import UpdateBoardDto from '../dto/board-update.dto';
import BoardInterface from '../interfaces/board.interface';
import BoardModel from '../models/board.model';
import UserModel from '../../users/user.model';
import ApiError from '../../utils/ApiError';
import StatusCodes from '../../utils/httpStatusCode';
import { toBoardDto } from '../mappers/board.mapper';
import ListModel from '../models/list.model';
import TaskModel from '../models/task.model';
import * as mongoose from 'mongoose';

export default class BoardService {
	private User = UserModel;
	private Board = BoardModel;
	private List = ListModel;
	private Task = TaskModel;

	async create(boardBody: CreateBoardDto, userId: string) {
		const newBoardBody: BoardInterface = {
			...boardBody,
			owner: userId,
		};
		const board = await this.Board.create(newBoardBody);
		return toBoardDto(board, []);
	}

	async findById(boardId: string) {
		const board = await this.Board.findById(boardId);
		if (!board) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found board with id ${boardId}`,
			);
		}

		const listLst = await this.List.find({ board: boardId });
		const lists = await Promise.all(
			listLst.map(async (list) => {
				const tasks = await this.Task.find({ list: list._id });
				return { list, tasks };
			}),
		);

		return { board: toBoardDto(board, lists), owner: board.owner };
	}

	async findAll() {
		const boards = await this.Board.find({});
		return boards;
	}

	async updateById(boardId: string, updateBody: UpdateBoardDto) {
		const board = await this.Board.findById(boardId);
		if (!board) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found board with id ${boardId}`,
			);
		}

		Object.assign(board, updateBody);
		await board.save();

		const listLst = await this.List.find({ board: boardId });
		const lists = await Promise.all(
			listLst.map(async (list) => {
				const tasks = await this.Task.find({ list: list._id });
				return { list, tasks };
			}),
		);

		return toBoardDto(board, lists);
	}

	async deleteBoard(boardId: string) {
		const board = await this.Board.findById(boardId);
		if (!board) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found board with id ${boardId}`,
			);
		}

		const session = await mongoose.startSession();
		session.startTransaction();
		try {
			const opts = { session };

			const lists = await this.List.find({ board: boardId });
			await Promise.all(
				lists.map(async (list) => {
					await this.Task.deleteMany({ list: list._id }, opts);
					await list.remove(opts);
				}),
			);

			await board.remove(opts);

			await session.commitTransaction();
			session.endSession();
			return true;
		} catch (e) {
			await session.abortTransaction();
			session.endSession();
			throw new Error(e);
		}
	}
}
