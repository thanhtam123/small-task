import TaskModel from '../models/task.model';
import ListModel from '../models/list.model';
import CreateTaskDto from '../dto/task-create.dto';
import StatusCodes from '../../utils/httpStatusCode';
import ApiError from '../../utils/ApiError';
import TaskInterface from '../interfaces/task.interface';
import { toTaskDto } from '../mappers/task.mapper';
import UpdateTaskDto from '../dto/task-update.dto';
import BoardModel from '../models/board.model';

export default class TaskService {
	private Board = BoardModel;
	private List = ListModel;
	private Task = TaskModel;

	async create(listBody: CreateTaskDto, listId: string) {
		const list = await this.List.findById(listId);
		if (!list) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found list with id ${listId}`,
			);
		}

		const newTaskBody: TaskInterface = {
			...listBody,
			list: list._id,
		};
		const task = await this.Task.create(newTaskBody);
		return toTaskDto(task);
	}

	async findById(taskId: string) {
		const task = await this.Task.findById(taskId);
		if (!task) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found task with id ${taskId}`,
			);
		}
		return toTaskDto(task);
	}

	async findTasksByListId(listId: string) {
		const tasks = await this.Task.find({ list: listId });
		return tasks.map((task) => toTaskDto(task));
	}

	async updateById(taskId: string, updateBody: UpdateTaskDto) {
		const task = await this.Task.findById(taskId);
		if (!task) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found task with id ${taskId}`,
			);
		}

		Object.assign(task, updateBody);
		await task.save();
		return toTaskDto(task);
	}

	async deleteTask(taskId: string) {
		const task = await this.Task.findById(taskId);
		if (!task) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found task with id ${taskId}`,
			);
		}

		await task.remove();
		return true;
	}

	async changeList(taskId: string, listId: string, boardId: string) {
		const task = await this.Task.findById(taskId);
		if (!task) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found task with id ${taskId}`,
			);
		}

		const oldList = await this.List.findById(task.list);
		if (!oldList) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found list with id ${task.list} of task with id ${taskId}`,
			);
		}
		if (oldList.board.toString() !== boardId.toString()) {
			throw new ApiError(
				StatusCodes.BAD_REQUEST,
				`Board with id ${boardId} does not contain task with id ${taskId}`,
			);
		}

		const board = await this.Board.findById(boardId);
		if (!board) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found board with id ${boardId}`,
			);
		}

		const list = await this.List.findById(listId);
		if (!list) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found list with id ${listId}`,
			);
		}
		if (list.board.toString() !== boardId.toString()) {
			throw new ApiError(
				StatusCodes.BAD_REQUEST,
				`List with id ${listId} does not belong to board with id ${boardId}`,
			);
		}

		task.list = listId;
		await task.save();
		return toTaskDto(task);
	}
}
