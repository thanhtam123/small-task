import ApiError from '../../utils/ApiError';
import StatusCodes from '../../utils/httpStatusCode';
import ListModel from '../models/list.model';
import BoardModel from '../models/board.model';
import CreateListDto from '../dto/list-create.dto';
import ListInterface from '../interfaces/list.interface';
import UpdateListDto from '../dto/list-update.dto';
import { toListDto } from '../mappers/list.mapper';
import TaskModel from '../models/task.model';
import * as mongoose from 'mongoose';

export default class ListService {
	private Board = BoardModel;
	private List = ListModel;
	private Task = TaskModel;

	async create(listBody: CreateListDto, boardId: string) {
		const board = await this.Board.findById(boardId);
		if (!board) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found board with id ${boardId}`,
			);
		}

		const newListBody: ListInterface = {
			...listBody,
			board: board._id,
		};
		const list = await this.List.create(newListBody);
		return toListDto(list, []);
	}

	async findById(listId: string) {
		const list = await this.List.findById(listId);
		if (!list) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found list with id ${listId}`,
			);
		}

		const tasks = await this.Task.find({ list: list._id });
		return toListDto(list, tasks);
	}

	async findListsByBoardId(boardId: string) {
		const lists = await this.List.find({ board: boardId });
		return lists;
	}

	async updateById(listId: string, updateBody: UpdateListDto) {
		const list = await this.List.findById(listId);
		if (!list) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found list with id ${listId}`,
			);
		}

		Object.assign(list, updateBody);
		await list.save();

		const tasks = await this.Task.find({ list: list._id });
		return toListDto(list, tasks);
	}

	async deleteList(listId: string) {
		const list = await this.List.findById(listId);
		if (!list) {
			throw new ApiError(
				StatusCodes.NOT_FOUND,
				`Not found list with id ${listId}`,
			);
		}

		const session = await mongoose.startSession();
		session.startTransaction();
		try {
			const opts = { session };

			await list.remove(opts);
			await this.Task.deleteMany({ list: listId }, opts);

			await session.commitTransaction();
			session.endSession();
			return true;
		} catch (e) {
			await session.abortTransaction();
			session.endSession();
			throw new Error(e);
		}
	}
}
