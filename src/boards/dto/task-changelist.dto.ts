import { IsNotEmpty, IsString } from 'class-validator';

export default class ChangeListTaskDto {
	@IsNotEmpty()
	@IsString()
	boardId: string;

	@IsNotEmpty()
	@IsString()
	listId: string;
}
