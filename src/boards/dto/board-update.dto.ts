import { IsOptional, IsString } from 'class-validator';

export default class UpdateBoardDto {
	@IsOptional()
	@IsString()
	title?: string;

	@IsOptional()
	@IsString()
	description?: string;
}
