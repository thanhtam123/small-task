export default class TaskDto {
	_id: string;
	title: string;
	description: string;
}
