import { IsOptional, IsString } from 'class-validator';

export default class UpdateListDto {
	@IsOptional()
	@IsString()
	title?: string;

	@IsOptional()
	@IsString()
	description?: string;
}
