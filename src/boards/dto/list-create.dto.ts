import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export default class CreateListDto {
	@IsNotEmpty()
	@IsString()
	title: string;

	@IsOptional()
	@IsString()
	description?: string;
}
