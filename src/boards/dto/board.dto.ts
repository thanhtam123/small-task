import ListDto from './list.dto';

export default class BoardDto {
	_id: string;
	title: string;
	description: string;
	lists: ListDto[];
	published: boolean;
}
