import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export default class CreateBoardDto {
	@IsNotEmpty()
	@IsString()
	title: string;

	@IsOptional()
	@IsString()
	description?: string;
}
