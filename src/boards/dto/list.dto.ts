import TaskDto from './task.dto';

export default class ListDto {
	_id: string;
	title: string;
	description: string;
	tasks?: TaskDto[];
}
