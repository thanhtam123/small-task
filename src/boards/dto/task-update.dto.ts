import { IsOptional, IsString } from 'class-validator';

export default class UpdateTaskDto {
	@IsOptional()
	@IsString()
	title?: string;

	@IsOptional()
	@IsString()
	description?: string;
}
