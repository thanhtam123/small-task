import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export default class CreateTaskDto {
	@IsNotEmpty()
	@IsString()
	title: string;

	@IsOptional()
	@IsString()
	description?: string;
}
