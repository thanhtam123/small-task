import TaskInterface from '@boards/interfaces/task.interface';
import * as mongoose from 'mongoose';

const taskSchema = new mongoose.Schema(
	{
		title: {
			type: String,
			trim: true,
			required: true,
		},
		description: {
			type: String,
			trim: true,
			default: 'Task description',
		},
		list: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'list',
			required: true,
		},
		// owner: {
		// 	type: mongoose.Schema.Types.ObjectId,
		// 	ref: 'User',
		// 	required: true,
		// },
		// members: {
		// 	type: [mongoose.Schema.Types.ObjectId],
		// 	ref: 'User',
		// 	default: [],
		// },
	},
	{
		timestamps: true,
	},
);

const TaskModel = mongoose.model<TaskInterface & mongoose.Document>(
	'Task',
	taskSchema,
);
export default TaskModel;
