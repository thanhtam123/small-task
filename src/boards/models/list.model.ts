import ListInterface from '@boards/interfaces/list.interface';
import * as mongoose from 'mongoose';

const listSchema = new mongoose.Schema(
	{
		title: {
			type: String,
			trim: true,
			required: true,
		},
		description: {
			type: String,
			trim: true,
			default: 'Task list description',
		},
		board: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Board',
			required: true,
		},
	},
	{
		timestamps: true,
	},
);

const ListModel = mongoose.model<ListInterface & mongoose.Document>(
	'List',
	listSchema,
);
export default ListModel;
