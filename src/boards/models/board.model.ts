import BoardInterface from '@boards/interfaces/board.interface';
import * as mongoose from 'mongoose';

const boardSchema = new mongoose.Schema(
	{
		title: {
			type: String,
			trim: true,
			required: true,
		},
		description: {
			type: String,
			trim: true,
			default: 'board description',
		},
		owner: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User',
		},
		published: {
			type: Boolean,
			default: false,
		},
	},
	{
		timestamps: true,
	},
);

const BoardModel = mongoose.model<BoardInterface & mongoose.Document>(
	'Board',
	boardSchema,
);
export default BoardModel;
