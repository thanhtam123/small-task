import ListDto from '@boards/dto/list.dto';
import TaskDto from '@boards/dto/task.dto';
import ListInterface from '@boards/interfaces/list.interface';
import TaskInterface from '@boards/interfaces/task.interface';
import { toTaskDto } from './task.mapper';

export const toListDto = (
	list: ListInterface,
	tasklist?: TaskInterface[],
): ListDto => {
	const { _id, title, description } = list;
	const tasks = tasklist.map((task) => toTaskDto(task));
	const ListDto: ListDto = { _id, title, description, tasks };
	return ListDto;
};
