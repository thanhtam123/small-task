import TaskDto from '@boards/dto/task.dto';
import TaskInterface from '@boards/interfaces/task.interface';

export const toTaskDto = (task: TaskInterface): TaskDto => {
	const { _id, title, description } = task;
	const taskDto: TaskDto = { _id, title, description };
	return taskDto;
};
