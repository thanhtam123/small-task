import BoardDto from '@boards/dto/board.dto';
import ListDto from '@boards/dto/list.dto';
import BoardInterface from '@boards/interfaces/board.interface';
import ListInterface from '@boards/interfaces/list.interface';
import TaskInterface from '@boards/interfaces/task.interface';
import { toListDto } from './list.mapper';

interface ListItem {
	list: ListInterface;
	tasks: TaskInterface[];
}

export const toBoardDto = (board: BoardInterface, listLst: ListItem[]) => {
	const { _id, title, description, published } = board;
	const lists = listLst.map((listItem) => {
		return toListDto(listItem.list, listItem.tasks);
	});
	const boardDto = { _id, title, description, published, lists };
	return boardDto;
};
