import UserInterface from '@users/user.interface';
import ListInterface from './list.interface';

export default interface TaskInterface {
	_id?: string;
	title: string;
	description?: string;
	list: string | ListInterface;
	// members: string[] | UserInterface[];
}
