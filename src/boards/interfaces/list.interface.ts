import BoardInterface from './board.interface';

export default interface ListInterface {
	_id?: string;
	title: string;
	description?: string;
	board: string | BoardInterface;
}
