import UserInterface from '@users/user.interface';

export default interface BoardInterface {
	_id?: string;
	title: string;
	description?: string;
	owner: string;
	published?: boolean;
}
