import BoardController from './controllers/board.controller';
import ListController from './controllers/list.controller';
import TaskController from './controllers/task.controller';
import BoardService from './services/board.service';
import ListService from './services/list.service';
import TaskService from './services/task.service';

export const boardService = new BoardService();
export const boardController = new BoardController(boardService);

export const listService = new ListService();
export const listController = new ListController(listService);

export const taskService = new TaskService();
export const taskController = new TaskController(taskService);
