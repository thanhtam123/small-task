import { Response, NextFunction } from 'express';
import RequestWithUser from '../interfaces/requestWithUser.interface';
import * as jwt from 'jsonwebtoken';
import DataStoredInToken from '../authentication/interfaces/dataStoredInToken';
import UserModel from '../users/user.model';
import ApiError from '../utils/ApiError';
import StatusCodes from '../utils/httpStatusCode';
import RequestWithAny from '../interfaces/requestWithAny.interface';
import BoardModel from '../boards/models/board.model';

export const verifyToken = async (
	req: RequestWithUser,
	res: Response,
	next: NextFunction,
) => {
	let token = req.headers['x-access-token'];

	if (!token) {
		return res
			.status(StatusCodes.FORBIDDEN)
			.send({ message: 'No token provide.' });
	}
	token = token.toString();

	const secret = process.env.JWT_SECRET;
	try {
		const verificationResponse = jwt.verify(
			token,
			secret,
		) as DataStoredInToken;
		const id = verificationResponse._id;
		const user = await UserModel.findById(id);
		if (user) {
			req.user = user;
			next();
		} else {
			next(new ApiError(StatusCodes.UNAUTHORIZED, 'Token is invalid.'));
		}
	} catch (error) {
		next(
			new ApiError(StatusCodes.UNAUTHORIZED, 'Error when verify token.'),
		);
	}
};

export const verifyTokenV2 =
	(requireLogin: boolean = false) =>
	async (req: RequestWithAny, res: Response, next: NextFunction) => {
		let token = req.headers['x-access-token'];

		if (!token) {
			if (requireLogin) {
				return res
					.status(StatusCodes.FORBIDDEN)
					.send({ message: 'No token provide. Require login!' });
			} else {
				req.isLogin = false;
				next();
			}
		} else {
			token = token.toString();

			const secret = process.env.JWT_SECRET;
			try {
				const verificationResponse = jwt.verify(
					token,
					secret,
				) as DataStoredInToken;
				const id = verificationResponse._id;
				const user = await UserModel.findById(id);
				if (user) {
					req.user = user;
					req.isLogin = true;
					next();
				} else {
					next(
						new ApiError(
							StatusCodes.UNAUTHORIZED,
							'Token is invalid.',
						),
					);
				}
			} catch (error) {
				next(
					new ApiError(
						StatusCodes.UNAUTHORIZED,
						'Error when verify token.',
					),
				);
			}
		}
	};

export const verifyAuth = async (
	req: RequestWithAny,
	res: Response,
	next: NextFunction,
) => {
	if (req.isLogin == true) {
		const { boardId } = req.body;
		req.body.boardId = undefined;
		const board = await BoardModel.findById(boardId);
		if (!board) {
			next(
				new ApiError(
					StatusCodes.NOT_FOUND,
					`Not found board with id ${boardId}`,
				),
			);
		} else if (board.owner.toString() != req.user._id.toString()) {
			next(
				new ApiError(
					StatusCodes.UNAUTHORIZED,
					'You are not authorized to access this board.',
				),
			);
		} else {
			next();
		}
	} else {
		next();
	}
};
